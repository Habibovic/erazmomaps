package com.amir.erazmomaps;


import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends AppCompatActivity {

    EditText username_text;
    EditText password_text;
    CallbackManager callbackManager;
    SharedPreferences.Editor editor;
    String facebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       //hashKey(); for facebook login hashkey

        setContentView(R.layout.activity_login);
        editor = getSharedPreferences("com.amir.erazmomaps", MODE_PRIVATE).edit();

        username_text = findViewById(R.id.username);
        password_text = findViewById(R.id.password);
        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getUserDetails(loginResult);
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Facebook login canceled!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(getApplicationContext(), "Error with facebook login!", Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //Facebook Login
    protected void getUserDetails(LoginResult loginResult) {
        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject json_object,
                            GraphResponse response) {
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        facebook = json_object.toString();
                        editor.putString("USER_PROFILE", facebook);
                        editor.putString("PROXIMITY_RADIUS", "2000"); //default radius value on preference
                        editor.putInt("MAP_TYPE", 4); //default map type on preference - hybrid
                        editor.apply();
                        startActivity(intent);
                        finish();
                    }

                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture.width(150).height(150)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();

    }

    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    //Form Login
    public void runLoggin(View view) {
        if (username_text.getText().toString().trim().length() > 0 && password_text.getText().toString().trim().length() > 0) {

            editor.putString("USERNAME", username_text.getText().toString());
            editor.putString("PASSWORD", password_text.getText().toString());
            editor.putString("PASSWORD", password_text.getText().toString());
            editor.putString("PROXIMITY_RADIUS", "2000"); //default radius value
            editor.putInt("MAP_TYPE", 4); //default map type on preference

            editor.putBoolean("IS_LOGGED", true);
            editor.apply();

            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        } else {
            Toast.makeText(this, "Please enter username and password", Toast.LENGTH_LONG).show();
            return;
        }
    }

    public void hashKey(){

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.amir.erazmomaps",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}

