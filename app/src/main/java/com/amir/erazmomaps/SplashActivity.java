package com.amir.erazmomaps;

import android.content.Intent;
import android.content.SharedPreferences;
import com.daimajia.androidanimations.library.Techniques;
import com.facebook.Profile;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

public class SplashActivity extends AwesomeSplash {

    Profile profile;
    SharedPreferences editor;
    Boolean isLogged;

    @Override
    public void initSplash(ConfigSplash configSplash) {

        editor = getSharedPreferences("com.amir.erazmomaps", MODE_PRIVATE);
        isLogged = editor.getBoolean("IS_LOGGED", false);
        //Facebook profile
        profile = Profile.getCurrentProfile();

        //Customize Circular Reveal
        configSplash.setBackgroundColor(R.color.yelow_trans); //any color you want form colors.xml
        configSplash.setAnimCircularRevealDuration(1000); //int ms
        configSplash.setRevealFlagX(Flags.REVEAL_RIGHT);  //or Flags.REVEAL_LEFT
        configSplash.setRevealFlagY(Flags.REVEAL_BOTTOM); //or Flags.REVEAL_TOP


        //Customize Logo
        configSplash.setLogoSplash(R.drawable.splash); //or any other drawable
        configSplash.setAnimLogoSplashDuration(2000); //int ms
        configSplash.setAnimLogoSplashTechnique(Techniques.FlipInX); //choose one form Techniques (ref: https://github.com/daimajia/AndroidViewAnimations)


        //Customize Title
        configSplash.setTitleSplash("Erazmo Maps");
        configSplash.setTitleTextColor(R.color.colorPrimaryDark);
        configSplash.setTitleTextSize(35f); //float value
        configSplash.setAnimTitleDuration(2000);
        configSplash.setAnimTitleTechnique(Techniques.FlipInX);


    }

    @Override
    public void animationsFinished() {
        //Check login
        if (isLogged || profile != null) {
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
        } else {
            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(i);;
        }
        // close activity
        finish();

           }
}
/*
public class SplashActivity extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    Profile profile;
    SharedPreferences editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        editor = getSharedPreferences("com.amir.erazmomaps", MODE_PRIVATE);
        final Boolean isLogged = editor.getBoolean("IS_LOGGED", false);
        //Facebook profile
        profile = Profile.getCurrentProfile();

        new Handler().postDelayed(new Runnable() {
			*/
/*
             * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 *//*

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if (isLogged || profile != null) {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);;
                }
                // close activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
*/

