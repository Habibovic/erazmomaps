/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.amir.erazmomaps.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.amir.erazmomaps.R;

import static android.content.Context.MODE_PRIVATE;

public class SettingsFragment extends Fragment {
    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    SeekBar radiusSeekBar;
    TextView textRadius;
    SharedPreferences.Editor editor;
    Button saveButton;
    RadioGroup mapTypeSelector;
    RadioButton normalMap, hybridMap, terrainMap, satelliteMap;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        radiusSeekBar = (SeekBar) getActivity().findViewById(R.id.radiusSeekBar); // initiate the progress bar
        textRadius = (TextView) getView().findViewById(R.id.textRadius);
        mapTypeSelector = (RadioGroup) getView().findViewById(R.id.mapTypeSelector);
        normalMap = getView().findViewById(R.id.normal_map);
        hybridMap = getView().findViewById(R.id.hybrid_map);
        terrainMap = getView().findViewById(R.id.terrain_map);
        satelliteMap = getView().findViewById(R.id.satellite_map);

        //Preference
        SharedPreferences prefs = getActivity().getSharedPreferences("com.amir.erazmomaps", MODE_PRIVATE);
        radiusSeekBar.setProgress(Integer.parseInt(prefs.getString("PROXIMITY_RADIUS", "2000")));

        //Check maptype
        int mapT = prefs.getInt("MAP_TYPE", 4);
        if (mapT == 1) {
            normalMap.setChecked(true);
        } else if (mapT == 2) {
            satelliteMap.setChecked(true);
        } else if (mapT == 3) {
            terrainMap.setChecked(true);
        } else if (mapT == 4) {
            hybridMap.setChecked(true);
        }

        //Seekbar
        textRadius.setText(String.valueOf(radiusSeekBar.getProgress()));
        radiusSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                // Log.i("TAG", "Seekbar changed" + radiusSeekBar.getProgress());
                i = i / 200;
                i = i * 200;
                textRadius.setText(String.valueOf(i));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        //On Click SAVE button
        saveButton = getView().findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs = getActivity().getSharedPreferences("com.amir.erazmomaps", MODE_PRIVATE);
                editor = prefs.edit();
                editor.putString("PROXIMITY_RADIUS", String.valueOf(textRadius.getText()));
                editor.apply();


                int selectedId = mapTypeSelector.getCheckedRadioButtonId();

                if (selectedId == normalMap.getId()) {
                    editor.putInt("MAP_TYPE", 1);
                    editor.apply();

                } else if (selectedId == satelliteMap.getId()) {
                    editor.putInt("MAP_TYPE", 2);
                    editor.apply();

                } else if (selectedId == terrainMap.getId()) {
                    editor.putInt("MAP_TYPE", 3);
                    editor.apply();

                } else if (selectedId == hybridMap.getId()) {
                    editor.putInt("MAP_TYPE", 4);
                    editor.apply();
                }
                }
            });


        }
    }
