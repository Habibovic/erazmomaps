/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.amir.erazmomaps.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amir.erazmomaps.LoginActivity;
import com.amir.erazmomaps.R;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment {
    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    TextView username_text;
    JSONObject response, profile_pic_data, profile_pic_url;
    Profile profile;
    SharedPreferences editor;
    String user_profile;
    Boolean isLogged;
    String username;
    String password;
    ImageView user_picture;
    Button logoutBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editor =  this.getActivity().getSharedPreferences("com.amir.erazmomaps", MODE_PRIVATE);

        username_text = getView().findViewById(R.id.username_text);
        profile = Profile.getCurrentProfile();
        logoutBtn = getView().findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //If facebook logedid
                if (profile != null) {
                    LoginManager.getInstance().logOut();
                } else {

                    SharedPreferences.Editor edit = editor.edit();
                    edit.clear();
                    edit.commit();

                }

                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        });


        username = editor.getString("USERNAME", "No name defined");//"No name defined" is the default value.
        password = editor.getString("PASSWORD", "No name defined");//"No name defined" is the default value.
        isLogged = editor.getBoolean("IS_LOGGED", false);//"No name defined" is the default value.
        user_profile = editor.getString("USER_PROFILE", "No name defined");
        user_picture = (ImageView) getView().findViewById(R.id.profilePic);

        checkLoginMethod();

    }

    public void checkLoginMethod() {
        //If facebook logedin
        if (profile != null) {
            String jsondata = user_profile;

            try {
                response = new JSONObject(jsondata);
                username_text.setText(response.get("name").toString());
                profile_pic_data = new JSONObject(response.get("picture").toString());
                profile_pic_url = new JSONObject(profile_pic_data.getString("data"));
                Picasso.with(this.getActivity()).load(profile_pic_url.getString("url"))
                        .into(user_picture);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            username_text.setText(username);
        }
    }

}
